---
title: Identificação de ruídos e redução de dimensionalidade para dados do DAF.
date: 2019-07-03 12:23:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]

tags:
---

Com a finalidade de se alcançar um conjunto de dados com qualidade suficiente para efetuar análises e predições com ceto grau de confiabilidade, são necessárias as aplicações de técnicas para identificação e remoção de outliers e também para seleção de atributos com maior peso nas tomadas de decisão sobre dispensação de medicamentos do Ministério da Saúde.

Nesse sentido, foram levantadas técnicas para aplicação no conjunto de dados e os resultados obtidos com as melhorias alcançadas na aplicação de algorítmos razos de aprendizagem, sendo estes também utilizados para a seleção de atributos com maior ganho de informação.

## Identificação de Outliers

Outliers são pontos fora da curva. Representam registros de dados anômalos aos que se pretende interpretar e consequentemente acarretam na perda de acurácia dos algoritmos de aprendizagem de máquina. Para a identificação e posterior remoção de outliers, foram estudados os seguintes modelos:
 - Matriz de Scatterplots
 - Z score
 - IQR inter-quartis

### Matriz de Scatterplots

O uso de scatterplots para visualização de dados é bastante comum e serve principalmente para análises visuais dos dados para adequação em proposição de curvas, avaliação de correlações e identificação de outliers. Para conjuntos de dados que seguem distribuições com menor variância, esta técnica é bastante eficaz. Entretanto para um conjunto de dados com maior variância, o limiar entre o que é um outlier e o que não é pode ser um problema.

A matriz de scatterplots permite visualizar, em um conjunto de dados multidimensionais não somente outliers, mas também correlações e modelos de distribuição dos dados.

![Scatterplot matrix](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-qualidade-dados/Captura_de_Tela_2019-07-03_%C3%A0s_13.21.22.png)

O uso de matrizes de scatterplots está normalmente vinculado a aplicação de normailização de dados (ou feature scaling) para padornizar as entradas e melhorar o formato de distribuição para análise gráfica. O exemplo da imagem anterior, após aplicação de normalização, com o uso de log normal, os dados ficam com melhor leitura visual, como ilustrado no exemplo abaixo.

![Matriz Normalizada](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-qualidade-dados/Captura_de_Tela_2019-07-03_%C3%A0s_13.46.23.png)

### Z Score

O uso de scatterplots para conjuntos de dados com maior dispersão pode gerar dificuldades na definição de outliers. Para resolver este problema, podem ser utilizadas outras técnicas como o Z Socre ou o IQR. O z Score permite calcular a distância em relação ao desvio padrão entre o dado observado e a média da distribuição. Caso a distância seja maior ou igual a 3, o valor pode ser considerado um outlier.

A fórmula para se calcular o fator Z para um ponto observado X é:

![Fórmula Z score](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-qualidade-dados/Captura_de_Tela_2019-07-03_%C3%A0s_13.56.00.png)

Com o uso combinado entre scatter e z-score, é possível selecionar os pontos que apresentam alguma dispersão e avaliar se ele se enquadra como outlier ou não.

### IQR inter-quartis

O fator IQR idica o grau de dispersão dos dados médios da distribuição. É considerado um outlier, qualquer ponto que se econtre inferior a 1.5 vezes o primeiro quartil ou superior a 1.5 vezes o terceiro quartil, como ilustra a figura.

![Inter quartis](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-qualidade-dados/Captura_de_Tela_2019-07-03_%C3%A0s_14.01.02.png)

Estas três técnicas foram selecionadas para, juntamente com a verificação da integridade dos dados, avaliar a qualidade para levantamentos preditivos e de aprendizado de máquina.

## Ganho de Informação

Além das avaliações estritamente do conjunto de dados, o objetivo futuro é aplicar algoritmos de aprendizagem rasa para avaliar também o ganho de informação de cada atributo registrado no banco para o contexto específico de predição de medicamentos. Nesse sentido, serão utilizados os seguintes algorítmos e técnicas para levantamento dos resultados tanto de remoção de outliers quanto de seleção de atributos com maior ganho de informação:
 - AdaBoost
 - Support Vector Machines
 
Ambos os algoritmos selecionados possuem a capacidade de indicar, segundo o aprendizado obtido, quais atributos de dados foram mais relevantes para as predições. Nesse sentido, além das métricas de acurácia, serão utilizados também as saídas de seleção de atributos para o levantamento das próximas estratégias de Machine Learning.

## Técnicas de Machine Learning
__[Para melhor compreensão da diferença entre regressão linear e redes neurais e demais técnicas de aprendizado de máquina de forma genérica e não apenas associada à qualidade de dados, clique aqui.](/msdaf/wiki/machine_learning/)__

### Aproximação com Clusterização por Atributo com Regressão Linear
A Clusterização é o agrupamento de cenários que tenham características parecidas. No caso do presente trabalho é o agrupamento de municípios com características parecidas. Com o uso da regressão é possível encontrar valores esperados para cada cluster e assim encontrar os outliers.
