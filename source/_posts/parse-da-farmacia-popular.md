---
title: Parser da Farmacia Popular
date: 2019-04-01 14:14:30
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---
## Manual de Geração do CSV
### Acessando site
* Acesse o site [Consultar Auditoria](http://consultaauditoria.saude.gov.br/) para a geração do arquivo CSV
![Site do SNA](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image01.png "Site do SNA")


### Filtros
* Selecione os filtros de acordo com a necessidade e clique em consultar.
![Selecione o Filtros de Consulta](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image02.png "Selecione o Filtros de Consulta")

### Obtendo a lista de Devolução
* Após localizar, selecione a opção DEVOLUÇÃO
![Selecione a Opção **Devolução**](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image03.png "Selecione a Opção **Devolução**")

### Nível de Agrupamento
* Na opção Nível de Agrupamento selecione **PREJUÍZO**
![Seleciona a opção Prejuízo](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image04.png "Seleciona a opção Prejuízo")

### Campos do CSV
* Abaixo, observe que há campos disponíveis para compor o CSV
![Localize as opções de seleção](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image07.png "Localize as opções de seleção")

#### Seleção de Campos
6. Caso esta geração seja com os débitos vinculados some te ao CNPJ
	1. Selecione os seguintes campos: **DATA DO FATO GERADOR, CNPJ, e Valor**
  ![Selecione as opções desejadas](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image05.png "Selecione as opções desejadas")

#### Seleção de Campos
7. Caso esta geração seja com os débitos vinculados para os representantes legais
	1. 1. Selecione os apenas seguintes campos: **DATA DO FATO GERADOR e Valor**
  ![Selecione as opções desejadas](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image08.png "Selecione as opções desejadas")

### Download do CSV
8. Clique na ícone do CSV para fazer o download do arquivo.
![Selecione as opções desejadas](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/parse-da-farmacia-popular-image06.png "Selecione as opções desejadas")

# Altere o arquivo CSV
Caso necessite realizar alteração ou organizar os registros no arquivo CSV exportado clique no manual abaixo.

[Manual para manipulação dos registros do CSV](https://gaesi.gitlab.io/msdaf/wiki/parser_ressarcimento_manual_CSV/)
