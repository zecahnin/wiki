---
title: Manual de Manipulação de Arquivo CSV
date: 2019-07-31 19:34:30
toc: true

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---
## Abrindo Arquivo CSV
Para abrir um arquivo CSV no excel com objetivo de organizar os registros basta seguir os passos abaixo:

- Caso não necessite do CNPJ do responsável para automatizar o preenchimento de sua razão social apenas exporte o CSV do site SNA marcando apenas as colunas DATA DO FATO GERADOR e VALOR, conforme explicado no manual de exportação item 7.

* Caso queira o preenchimento da razão social do responsável automaticamente execute os seguintes passos:
	1. Criar uma nova planilha de dados vazia;

	2. Pressione o menu Arquivo -> Importar…;
  ![](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/manual_ressarcimento_csv/img1.png)

	3. Na caixa de opções que abrirá selecione o tipo “Arquivo CSV” em seguida clique no botão Importar;
  ![](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/manual_ressarcimento_csv/img2.png)

	4. Na caixa de seleção localize o arquivo exportado pelo site do SNA, selecione-o e clique no botão “Obter Dados”;

	5. Em seguida abrirá um assistente de importação com 3 passos, no primeiro selecione a opção “Delimitado …” em seguida no botão Avançar;
	6. No segundo passo selecione a opção “Ponto-e-Virgula” e observe se os campos serão divididos em colunas na visualização na parte inferior da caixa, em seguida clique em Avançar;
  ![](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/manual_ressarcimento_csv/img3.png)

	7. A terceira etapa é uma das principais, selecione na visualização dos dados a coluna de CNPJ e nas opções acima selecione TEXTO;
	![](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/manual_ressarcimento_csv/img4.png)
	8. Então selecione a coluna DATA FATO GERADOR e selecione a opção DATA deixando escolhida a opção DMA ( Dia / Mês / Ano), para finalizar clique em Concluir;  
  ![](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/manual_ressarcimento_csv/img5.png)

	8. Abrirá uma caixa para que indique qual célula deseja incluir o conteúdo importado, preencha com a célula A1 da planilha criada e clique em OK, esta seleção é muito importante para o processo de conversão;

Finalizado o processo, observe que o CNPJ foi importado de forma correta e a DATA FATO GERADOR não possui mais as ASPAS.

Faça as organizações dos registro conforme sua necessidade mantendo apenas a ordem das colunas.

Ao finalizar Salve o arquivo trocando a extensão para “CSV Separado por Ponto-e-Virgula”.

Após salvar importe o arquivo na ferramenta de conversão.
