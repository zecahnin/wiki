---
title: Processo de multa e ressarcimento - Especificação Técnica
date: 2019-08-02 11:57:01

toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left
    -
        type: tag
        position: left
tags:
 - entrega

categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

## Objetivo
Desenvolver documentação do subprocesso de multa e ressarcimento, indentificando pontos de melhoria e possíveis automações com uso de recursos tecnológicos como suporte.

## Visão
O subprocesso de multa e ressarcimento está contido no programa Farmácia Popular, responsável por identificar, notificar e em último caso multar ou ressarcir farmácias registradas. Devido à existência de certo volume passivo de indícios em aberto para análise, o processo de Multa e Ressarcimento foi identificado como prioritário para avaliação e melhoria.

## Escopo
 - Indentificação de validação dos procedimentos atuais relacionados ao processo de multa e ressarcimento
 - Documentação dos procedimentos padrões do processo
 - Identificação de gargalos, mudanças ou oportinudades de melhoria do processo
 - Desenvolvimento de protótipos para avaliação das melhorias propostas
 - Documentação técnica e funcional dos protótipos desenvilvidos

## Processo

![Modelo Multa Ressarcimento v2.png](https://gaesi.gitlab.io/msdaf/wiki/images/entrega-multa-ressarcimento/Modelo_MultaRessarcimento_v2.png "Modelo Multa Ressarcimento")

#### Objetivo do sub-processo: garantir o ressarcimento de valores pagos pelo erário à farmácias participantes do Programa Farmácia Popular do Brasil que cometeram atos contrários ao exercício do programa.

O processo de ressarcimento se inicia com um documento chamado “Relatório de Auditoria” (RA), construído pelo Denasus com um conjunto de informações relativas à auditoria realizada em uma determinada farmácia participante dentro de um determinado período de tempo, farmácia essa que, após solicitação de documentos anterior, não conseguiu justificar determinadas transações que fogem à portaria regulamentadora do programa.

Esse RA pode ser motivado por duas razões principais:
 1. Por sinalização de indício de fraude advinda da equipe de monitoramento, indícios esses materializados após a farmácia não conseguir comprovar documentalmente a correta dispensação de medicamentos em um conjunto de transações suspeitas;
 2. Por motivação de outros órgãos de controle e auditoria externos ao MS;

O procedimento de instauração de averiguação pelo Denasus tem origem em após o Denasus enviar um ofício para uma farmácia participante (e seus responsáveis legais no intervalo da auditoria) solicitando a comprovação documental de um número de transações com indícios de fraude/irregularidade ao programa, e esse conjunto de pessoas físicas e jurídicas não retornar comprovantes de que essas transações foram realizadas dentro das premissas do programa. Ao final desse procedimento de instauração, constrói-se o RA.

Posterior à não completa resposta a esses questionamentos, inicia-se o processo de ressarcimento e/ou de descredenciamento da farmácia participante. Nessa fase, o fluxo se desdobra entre as ações de penalidade e ações de ressarcimento ao erário. Dependendo do tipo de irregularidades encontradas, podem-se tomar ações sumárias de descredenciamento do programa. Se a ação não ensejar essa atitude, então a equipe decide em uma multa a ser aplicada à farmácia pelas irregularidades encontradas até o valor de 10% do valor do dano causado. Toda farmácia que passa por esse processo é passível de sofrer multa dada a irregularidade.

Sobre a fase de ressarcimento, primeiro é necessário validar o RA recebido em termos de completude dos documentos, validação do nexo de causalidade entre os envolvidos e seus tempos de gestão administrativa (não técnica) da farmácia e a presença de todas as notificações do Denasus aos responsáveis auditados. Caso haja alguma falha documental detectada, é necessário retornar o RA ao Denasus para preenchimento das lacunas identificadas. Após essa validação primária do documento e a completude das informações comprovada, parte-se para a notificação (feita pelo DAF) ao CNPJ e CPFs envolvidos, no estrito limite do nexo temporal e causal de cada um. É importante lembrar que todos os sócios da farmácia, no limite das suas gestões até a data-fim da auditoria, respondem solidariamente por todo o processo. 

Antes de construir a notificação, é necessário consultar o SISGRU para verificar se não houve nenhum pagamento de ressarcimento anterior à notificação da parte de algum dos CPFs envolvidos ou em nome do CNPJ em auditoria. Caso haja pagamento total, o processo termina aqui. Caso haja pagamento parcial, os valores restantes serão atualizados e cobrados conforme o fluxo.

A notificação é construída a partir das seguintes informações: 

 1. informações pessoais da farmácia  (CNPJ) e dos CPFs envolvidos (obtida via SISTCE e comprovada via consultas no site da receita federal). Se a farmácia tiver um CNPJ ativo, esse será também notificado, em cima das notificações pessoais que cada um dos sócios recebe por CPF. Os CPFs são validados junto à Receita Federal para verificação da validade dos mesmos e também para saber se algum dos sócios veio a óbito no decorrer do período até a notificação (caso tenha sido, o processo corre contra os herdeiros);
 2. informações do Denasus sobre as glosas, ou seja, as transações realizadas com indícios materializados de fraude/irregularidade;
 3. informações sobre o domínio do débito, obtidas através de atualização monetária dos valores das glosas desde a data do fato até a data da notificação;
 4. GRU de pagamento dos valores (montante principal+atualização via SELIC, se for o caso), gerada via SISGRU. Essa GRU é gerada sempre com validade até o fim do mês de recebimento da notificação.

É necessária cautela na geração dessas informações, dado que o TCU é criterioso na garantia da ampla defesa e do contraditório aos responsáveis durante todo o processo. Caso o TCU não identifique a garantia desses direitos ou haja alguma incoerência/falta de informação, o processo retorna ao DAF com prazo marcado para reenvio ao TCU.

Todas as informações precisam estar relacionadas no SEI ao numero do processo envolvendo a auditoria. Todos os envios feitos devem ter seus Avisos de Recebimento e/ou devoluções escaneados e anexados ao processo. Caso alguma notificação não seja recebida pelo responsável, este deverá ser chamado via edital publicado no DOU. Apenas então pode-se considerar que a pessoa está ciente do processo aberto contra ela.

Caso a notificação não seja respondida/pagamento não tenha sido realizado, parte-se para a criação de uma nota técnica contendo todas as informações relativas ao processo, atualizadas até a data de emissão da Nota, também atualizando o valor do débito relativo ao processo.Assim sendo,o DAF solicita uma instauração de tomada de contas especial para o Fundo Nacional de Saúde que, por sua vez, como instaurador, solicita uma tomada de contas especial ao TCU, se o montante for maior do que R$ 100.000,00. Caso o montante seja menor do que este, o DAF encaminha por meio de despacho todas as informações necessárias para que o FNS realize o cadastramento de débito em aberto no Sistema e-TCE, para posterior encaminhamento à Procuradoria Geral da Fazenda Nacional, visando o acionamento judicial das partes.

#### Objetivo do sub-processo: penalizar as farmácias participantes do Programa Farmácia Popular do Brasil que cometeram atos contrários ao exercício do programa.

Após a chegada do RA, de autoria do Denasus, e separação entre as ações de restituição e penalidade, analisa-se a completude das informações do RA segundo à visão das possíveis penalidades (completude dos documentos necessários segundo a gravidade das ofensas). De acordo com uma primeira análise do RA, existe um conjunto de ofensas que permitem ao DAF prosseguir com o descredenciamento sumário de uma determinada farmácia (lembrando, sempre, que matrizes e filiais respondem como entes diferentes para o credenciamento: o descredenciamento de uma matriz não acarreta o descredenciamento das filiais, e vice-versa).

 - Hipóteses passíveis de descredenciamento sumário:
 - Descredenciamento por pedido judicial;
 - Descredenciamento a pedido do responsável legal;
 - Descredenciamento por irregularidades;
 - Descredenciamento por não-homologação dentro do período de testes
 - Descredenciamento por não renovação do RTA

Dentro do descredenciamento por irregularidades, é importante frisar o limite/barreira dos 16%. Esse limite implica que, se, durante uma determinada auditoria, mais de 16% do valor total dispensado entre (M-4) e (M-1) for irregular, a farmácia é passível de ser descredenciada sumariamente. No limiar dos 16%, é importante analisar o cenário completo da farmácia: reincidências, sócios em situação irregular com o DAF e situações semelhantes podem embasar um possível descredenciamento sumário. Em casos extremos, pode-se consultar a gestão do programa para tomada de decisão.

Micro serviços

Para o processo de multa e ressarcimento foi desenvolvido um estudo para utilização de micro serviço integrador ao SISGRU para simplificar o processo de geração de Guias de recolhimento e para centralizar o trabalho em uma única ferramenta.

Especificação
