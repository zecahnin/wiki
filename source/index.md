---
title: Projeto de Pesquisa e Inovação para o DAF/MS
date: 2019-03-11 12:09:42
---
O **[GAESI](http://www.gaesi.eng.br)** em parceria com o **[Departamento de Assistência Farmacêutica do Ministério da Saúde](http://portalms.saude.gov.br/assistencia-farmaceutica)** desenvolvem estratégias tecnológicas para permitir, em sua esfera finalística, maior acesso dos cidadãos a medicamentos para recuperação e redução dos riscos de doenças e suas sequelas. Essas estratégias incluem a redução no número de fraudes relacionandas à distribuição de medicamentos, otimização dos processos de distribuição, e melhoria nos processos internos de gestão de dados relacionados a [oferta de medicamentos do SUS](/about/#A-assistencia-Farmaceutica). Esta plataforma é uma centralização dos esforços de explanação das pesquisas, códigos-fonte e resultados obtidos nos estudos realizados pelo grupo do GAESI atuante no Ministério da Saúde.

## Atividades

A equipe executa hoje atividades relacionandas a três grandes áreas:

 * **Análise de dados** referentes a planilhas, bancos de dados e quaisquer outros artefatos pertinentes para o estudo dos processos inseridos no contexto do DAF.
 * **Desenvolvimento** de ferramentas e aplicações para carregamento, processamento e análise computacional a fim de diminuir processos burocráticos e facilitar a identificação de informações críticas.
 * **Estudos científicos** empenhados na documentação e registro formal dos processos e técnicas utilizados para cada resultado obtido, com a finalidade de gerar pesquisas replicáveis e úteis para o meio acadêmico.

 Todas as atividades executadas podem ser acompanhadas pelo nosso [quadro de atividades](https://gitlab.com/groups/gaesi/msdaf/-/boards).

## [Ecossistema](/msdaf/wiki/repositories)

As equipes divididas nas frentes de trabalho apresentadas utilizam o seguinte conjunto de ferramentas para trabalho:
 * **Análise de dados:**
    - Google docs: Para trabalhos de produção textual e mateiriais de estudo da equipe.
    - Google sheets: Para manuseio de tabelas e trabalho coletivo de normailização e identificação de dados.
    - [Python 3](https://www.python.org/downloads/): Extração e processamento de dados para persistência em banco de dados ou arquivos `.csv`.
    - [DBEaver](https://dbeaver.io/): Para acesso ao banco de dados Oracle.

 * **Desenvolvimento**
    - [Python 3](https://www.python.org/downloads/): Linguagem para desenvolver principais módulos de transcrição e exportação de dados.
    - [Django](https://www.djangoproject.com/): Framework para desenvolvimento de API REST para integração dos micro serviços.
    - [Flesk](http://flask.pocoo.org/): Framework para desenvolvimento dos micro serviços individualmente.
    - [Netflix OSS](https://netflix.github.io/): *Runtime* completo para ambientes de micro serviços.
    - [Angular 7](https://angular.io/): Framework frontend para desenvolvimento de interfaces.

 * **Estudos Científicos**
    - [Python 3](https://www.python.org/downloads/): desenvolvimento de estudos e plotagens gráficas.
    - [Jupyter](https://jupyter.org/): Para desenvolvimento de documentos interativos para visualização e execução de código.
    - [TensorFlow](https://www.tensorflow.org/): Desenvolvimento de modelos e treinamento de clasificadores.
    - [Keras](https://keras.io/)/[Pandas](https://pandas.pydata.org/)/[SKlearn](https://scikit-learn.org/stable/): Ferramentas auxiliares para construção e visualização de datasets em exemplos educativos.


