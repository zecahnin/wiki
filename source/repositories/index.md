---
title: Ecossistema
date: 2019-03-11 12:26:32
toc: true

sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

categories:
- [Repositórios, Código, Parsers]
- [Repositórios, Código, Wiki]
- [Repositórios, Código, Microservices]
- [Repositórios, Código, AI]
- [Repositórios, Código, Exploratory Analisys]
- [Repositórios, Código, UI]
- [Repositórios, Código, Runtime Images]
- [Repositórios, Código, Config]
---

O ecossistema de ferramentas e aplicações utilizadas no desenvolvimento tanto a nível de código quanto produção científica e textual são apresentados a seguir.

## Ambientes

### Ambientes individuais

Os ambientes de trabalho pessoal utilizam as seguintes ferramentas para cada frente de trabalho:

* **Análise de dados:**
    - Google docs: Para trabalhos de produção textual e mateiriais de estudo da equipe.
    - Google sheets: Para manuseio de tabelas e trabalho coletivo de normailização e identificação de dados.
    - [Python 3](https://www.python.org/downloads/): Extração e processamento de dados para persistência em banco de dados ou arquivos `.csv`.
    - [DBEaver](https://dbeaver.io/): Para acesso ao banco de dados Oracle.

 * **Desenvolvimento**
    - [Python 3](https://www.python.org/downloads/): Linguagem para desenvolver principais módulos de transcrição e exportação de dados.
    - [Django](https://www.djangoproject.com/): Framework para desenvolvimento de API REST para integração dos micro serviços.
    - [Flesk](http://flask.pocoo.org/): Framework para desenvolvimento dos micro serviços individualmente.
    - [Netflix OSS](https://netflix.github.io/): *Runtime* completo para ambientes de micro serviços.
    - [Angular 7](https://angular.io/): Framework frontend para desenvolvimento de interfaces.

 * **Estudos Científicos**
    - [Python 3](https://www.python.org/downloads/): desenvolvimento de estudos e plotagens gráficas.
    - [Jupyter](https://jupyter.org/): Para desenvolvimento de documentos interativos para visualização e execução de código.
    - [TensorFlow](https://www.tensorflow.org/): Desenvolvimento de modelos e treinamento de clasificadores.
    - [Keras](https://keras.io/)/[Pandas](https://pandas.pydata.org/)/[SKlearn](https://scikit-learn.org/stable/): Ferramentas auxiliares para construção e visualização de datasets em exemplos educativos.

### Ambientes de teste

O ambiente de teste das aplicações desenvolvidas e resultados obtidos nos estudos e análises são:

 * **Aplicações** - Ambiente de homologação disponibilizado pelo Ministério da Saúde contendo as seguintes aplicalções:
    - [Python 3](https://www.python.org/downloads/): Linguagem para desenvolver principais módulos de transcrição e exportação de dados.
    - [Django](https://www.djangoproject.com/): Framework para desenvolvimento de API REST para integração dos micro serviços.
    - [Flesk](http://flask.pocoo.org/): Framework para desenvolvimento dos micro serviços individualmente.
    - [Netflix OSS](https://netflix.github.io/): *Runtime* completo para ambientes de micro serviços.
    - [Angular 7](https://angular.io/): Framework frontend para desenvolvimento de interfaces.
    - [Docker](https://docker.io/): Plataforma de *containers* para construir, gerenciar e evoluir aplicações para *deploy* em direfentes ambientes.

 * **Artigos e documentos** - Ambiente para disponibilização de versões intermediárias para avaliação e validação:
    - [Wiki oficial](https://gaesi.gitlab.io/msdaf/wiki/): Repositório oficial dos produtos desenvolvidos/em desenvolvimento pela equipe.
    - [hexo](https://hexo.io/pt-br/): Aplicação para construir localmente a wiki para permitir novas publicações. É necessário ter conhecimentos básicos em Javascript e Markdown.
    - [Python 3](https://www.python.org/downloads/): desenvolvimento de estudos e plotagens gráficas.
    - [Jupyter](https://jupyter.org/): Para desenvolvimento de documentos interativos para visualização e execução de código.

### Ambiente de produção

Ainda não foram definidas estratégias para disponibilização das aplicações desenvolvidas, que serão inicialmente acessadas pelo ambinete de teste e homologação.

## [Repositórios](/msdaf/wiki/categories/Repositorios)

Para compreender melhor como estão organizados os trabalhos é importante entender os repositórios e suas finalidades específicas.

### Runtime Images

Conjunto de imagens Docker para configurar e inicializar o ambiente no qual os serviços desenvolvidos serão executados. Para saber mais sobre e a 
arquitetura das aplicações disponibilizadas, [acesse aqui](/msdaf/wiki/architecture). Para acessas as imagens construídas, acesse o
[repositório](https://gitlab.com/gaesi/msdaf/netflixoss-images) ou o [DockerHub](https://hub.docker.com).

### Config

Arquivos de configuração necessários para o ambeinte de *microservices*. É importante conhecer a arquitetura e aplicações que são inicializadas
pelo *runtime* para compreender melhor as configurações utilizadas.

### Microservices

Conjunto de micro servços que são disponibilizados na infraestrutura de homologação. Para disponibilizar um novo serviço, siga para o
[tutorial de criação de novos serviços](/msdaf/wiki/new-microservice)

### Parsers

Existem inúmeros conjuntos de dados que não estão sistematizados no Ministério da Saúde. Para permitir integração desses dados nas análises,
foram desenvolvidos parsers que estão disponíveis [aqui](https://gitlab.com/gaesi/msdaf/parsers)

### Wiki

[Repositório](https://gitlab.com/gaesi/msdaf/wiki) para os códigos-fonte desta wiki.

### Exploratory Analisys
Análise exploratória dos dados históricos relacionados ao programa Farmácia Popular e a predição de estoque dos fármacos vinculados ao programa.


### AI



### UI


-----